"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#
brevetException = [[200, 810, 353], 
                        [300, 1200, 540], 
                        [400, 1620, 728], 
                        [600, 2400, 1128], 
                        [1000, 4500, 1985]]
speedData = [[0, 15, 34], 
             [200, 15, 32], 
             [400, 15, 30], 
             [600, 11.428, 28]]

def calculation(control_dist_km, brevet_dist_km, open):
   timeDiff = 0
   pastDistance = 0
   index = 2 if open else 1

   if ((control_dist_km > (brevet_dist_km * 1.2)) or (control_dist_km < 0)): #return 0 if working
      return 0 
   if (control_dist_km >= brevet_dist_km):
      for i in range(0, len(brevetException)):
         if (brevet_dist_km == brevetException[i][0]):
            return (brevetException[i][index]) #return it if it's equal
   if not open and (control_dist_km < 60):
      return round((control_dist_km / 20 * 60) + 60)
   for i in range(len(speedData)-1, -1, -1):
      if (control_dist_km > speedData[i][0]):
         speed = (speedData[i][index])
         temp = (control_dist_km - pastDistance - speedData[i][0])
         pastDistance += temp
         timeDiff += (temp / speed) * 60
   return (round(timeDiff)) #return the rounded difference in time. Right? come back to this austin

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, control distance in kilometers
      brevet_dist_km: number, nominal distance of the brevet
         in kilometers, which must be one of 200, 300, 400, 600,
         or 1000 (the only official ACP brevet distances)
      brevet_start_time:  A date object (arrow)
   Returns:
      A date object indicating the control open time.
      This will be in the same time zone as the brevet start time.
   """
   time = calculation(control_dist_km, brevet_dist_km, True) #true because of open
   brevet_start_time = brevet_start_time.shift(minutes=(time % 60))
   brevet_start_time = brevet_start_time.shift(hours=(time // 60))
   return (brevet_start_time)


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, control distance in kilometers
        brevet_dist_km: number, nominal distance of the brevet
        in kilometers, which must be one of 200, 300, 400, 600, or 1000
        (the only official ACP brevet 
      brevet_start_time:  A date object (arrow)
   Returns:
      A date object indicating the control close time.
      This will be in the same time zone as the brevet start time.
   """
   time = calculation(control_dist_km, brevet_dist_km, False) #false because of close
   brevet_start_time = brevet_start_time.shift(minutes=(time % 60))
   brevet_start_time = brevet_start_time.shift(hours=(time // 60))
   return (brevet_start_time)
