CIS 322 Project 4

Project: An ACP Brevet control calculator implemented with the use of frontend AJAX and Flask.
Author: Austin Mohler

Contact: amohler@uoregon.edu

For the User: 
If the Control Distance of the Brevet Distance is greater than 120% , it will return the given starting date with no change of the open and close time.

If the Control Distance is greater than the Brevet Distance's i.e. (200, 300, 400, 600, 1000) and within 120% of the Brevet Distance, it will return the same starting and closing time.

If the Control Distance is negative, it will return the given starting date, and there will be no change to open and close.

If the Control Distance is equal to 0, the opening time will stay the same, but the closing time is 1 hour ahead of the starting time.

If the Control Distance is less than 60 and greater than 0, the closing time will use 20 km/hr, and there will be an extra hour added on for calculations.

If the Control Distance is equal to the Brevet Distance, the speed will be calculated using the control speed of the lower.

For the Developer:
In order to run these nosetests, you must be in the "brevets" folder, and you must execute "nosetests tests/test_asp_times.py".

For my teachers:
I noticed an error in my "calc.html" that never ended up causing any issues, I just wanted to bring this to attention.
var SCRIPT_ROOT = {{ request.script_root|tojson|safe }} ;
^^^ This line to be specific shows up as an error. Still noticed this even after the updated code.
I believe I fixed the acp_times.py problem, thanks again for the second chance!